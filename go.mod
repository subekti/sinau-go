module github.com/Sigit-Wasis/Gofiber

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.31.0
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/valyala/fasthttp v1.35.0 // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/sys v0.0.0-20220405052023-b1e9470b6e64 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/driver/postgres v1.3.3
	gorm.io/gorm v1.23.4
)
